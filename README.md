# weatherwarnings


This repository scrapes the latest weather warning GeoJSON from the MetOffice web
pages and drops it [here](https://b-rowlingson.gitlab.io/weatherwarnings/latest-warnings.geojson)

See the [web pages](https://b-rowlingson.gitlab.io/weatherwarnings/) for more info.

